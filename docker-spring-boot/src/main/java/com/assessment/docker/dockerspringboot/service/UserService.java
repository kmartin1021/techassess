package com.assessment.docker.dockerspringboot.service;

import com.assessment.docker.dockerspringboot.model.User;

import java.util.List;

public interface UserService {

    public List<User> getAllUser() throws Exception;
    public User getUserById(long id) throws Exception;
    public void saveOneUser(User user) throws Exception;
}
