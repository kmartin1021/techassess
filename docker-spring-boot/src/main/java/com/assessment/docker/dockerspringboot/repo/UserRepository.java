package com.assessment.docker.dockerspringboot.repo;

import com.assessment.docker.dockerspringboot.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByFirstName (String firstName);
    User findByLastName (String lastName);
    User findByDescription(String description);

}
