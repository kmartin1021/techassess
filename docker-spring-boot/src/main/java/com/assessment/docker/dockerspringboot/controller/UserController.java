package com.assessment.docker.dockerspringboot.controller;

import com.assessment.docker.dockerspringboot.model.User;
import com.assessment.docker.dockerspringboot.repo.UserRepository;
import com.assessment.docker.dockerspringboot.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/")
    public String home() {
        return "User - Demo Spring Boot";
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<User> getAllUser() throws Exception {

        List<User> users = userService.getAllUser();

        return users;
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public User getOneUserByFirstName(@PathVariable Long id) throws Exception {

        User user = userService.getUserById(id);

        return user;
    }
    @PostMapping(value = "/add", produces = MediaType.APPLICATION_JSON_VALUE)
    public User saveOneUser(@RequestBody User user) throws Exception {

        userService.saveOneUser(user);

        return user;
    }
}
