package com.assessment.docker.dockerspringboot.service.Impl;

import com.assessment.docker.dockerspringboot.model.User;
//import com.assessment.docker.dockerspringboot.repo.UserRepository;
import com.assessment.docker.dockerspringboot.repo.UserRepository;
import com.assessment.docker.dockerspringboot.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    public List<User> getAllUser() throws Exception {
        List<User> users = userRepository.findAll();

        return users;
    }

    public User getUserById(long id) throws Exception{
        User user = userRepository.findById(id).orElse(null);

        return user;
    }

    public void saveOneUser(User user) throws Exception{
        userRepository.save(user);
    }
}
